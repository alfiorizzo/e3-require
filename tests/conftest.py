import os
import subprocess
from pathlib import Path
from random import choice
from string import ascii_lowercase

import pytest
from git import Repo


class Wrapper:
    def __init__(self, root_path: Path, name=None, include_dbd=True, **kwargs):
        if name is None:
            name = "test_mod_" + "".join(choice(ascii_lowercase) for _ in range(16))
        self.path = root_path / f"e3-{name}"
        self.name = name

        module_path = (
            name if "E3_MODULE_SRC_PATH" not in kwargs else kwargs["E3_MODULE_SRC_PATH"]
        )
        self.module_dir = self.path / module_path
        self.module_dir.mkdir(parents=True)

        self.config_dir = self.path / "configure"
        self.config_dir.mkdir()

        self.config_module = self.path / "CONFIG_MODULE"
        self.config_module.touch()

        self.makefile = self.path / f"{name}.Makefile"

        makefile_contents = f"""
TOP:=$(CURDIR)

E3_MODULE_NAME:={name}
E3_MODULE_VERSION?=0.0.0+0
E3_MODULE_SRC_PATH:={module_path}
E3_MODULE_MAKEFILE:={name}.Makefile

include $(TOP)/CONFIG_MODULE

include $(REQUIRE_CONFIG)/CONFIG
include $(REQUIRE_CONFIG)/RULES_SITEMODS
"""
        with open(self.path / "Makefile", "w") as f:
            f.write(makefile_contents)

        module_makefile_contents = """
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
"""
        with open(self.path / f"{name}.Makefile", "w") as f:
            f.write(module_makefile_contents)

        if include_dbd:
            self.add_file("test.dbd")

        Repo.init(self.path)

    def add_file(self, name):
        (self.module_dir / name).touch()

    def write_dot_local_data(self, config_file: str, config_vars: dict):
        """Write config data to the specific .local file."""

        with open(self.config_dir / f"{config_file}.local", "w") as f:
            for var, value in config_vars.items():
                f.write(f"{var} = {value}\n")

    def add_var_to_config_module(self, makefile_var: str, value: str, modifier="+"):
        with open(self.config_module, "a") as f:
            f.write(f"{makefile_var} {modifier}= {value}\n")

    def add_var_to_makefile(self, makefile_var: str, value: str, modifier="+"):
        with open(self.makefile, "a") as f:
            f.write(f"{makefile_var} {modifier}= {value}\n")

    def get_env_var(self, var: str):
        """Fetch an environment variable from the module build environment."""

        _, out, _ = self.run_make("cellvars")
        for line in out.split("\n"):
            if line.startswith(var):
                return line.split("=")[1].strip()
        return ""

    def run_make(
        self,
        target: str,
        *args,
        module_name: str = "",
        module_version: str = "",
        cell_path: str = "",
    ):
        """Attempt to run `make <target> <args>` for the current wrapper."""

        test_env = os.environ.copy()

        assert "EPICS_BASE" in test_env
        assert Path(test_env["EPICS_BASE"]).is_dir
        assert "E3_REQUIRE_VERSION" in test_env
        assert test_env["E3_REQUIRE_VERSION"]

        e3_require_location = (
            Path(os.environ.get("EPICS_BASE"))
            / "require"
            / os.environ.get("E3_REQUIRE_VERSION")
        )
        assert e3_require_location.is_dir()
        test_env["E3_REQUIRE_LOCATION"] = e3_require_location

        require_config = f"{e3_require_location}/configure"
        test_env["REQUIRE_CONFIG"] = require_config

        if module_name:
            test_env["E3_MODULE_NAME"] = module_name
        if module_version:
            test_env["E3_MODULE_VERSION"] = module_version
        if cell_path:
            self.write_dot_local_data("CONFIG_CELL", {"E3_CELL_PATH": cell_path})
        make_cmd = ["make", "-C", self.path, target] + list(args)
        p = subprocess.Popen(
            make_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            env=test_env,
            encoding="utf-8",
        )
        outs, errs = p.communicate()
        return p.returncode, outs, errs


@pytest.fixture
def wrappers(tmpdir):
    class WrapperFactory:
        def get(self, **kwargs):
            return Wrapper(Path(tmpdir), **kwargs)

    yield WrapperFactory()


@pytest.fixture
def wrapper(wrappers):
    yield wrappers.get()
