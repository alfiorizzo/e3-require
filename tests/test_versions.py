import re

import pytest

from .utils import run_ioc_get_output

RE_MODULE_LOADED = "Loaded {module} version {version}"
RE_MODULE_NOT_LOADED = "Module {module} (not available|version {required} not available|version {required} not available \\(but other versions are available\\))"


@pytest.mark.parametrize(
    "requested, expected, installed",
    [
        # If nothing is installed, nothing should be loaded
        ("", "", []),
        # Test versions can be loaded
        ("test", "test", ["test", "0.0.1"]),
        # Numeric versions should be prioritized over test versions
        ("", "0.0.1+0", ["test", "0.0.1"]),
        ("", "0.0.1+0", ["0.0.1", "test"]),
        # Highest build number should be loaded if version is unspecified
        ("", "0.0.1+7", ["0.0.1", "0.0.1+7", "0.0.1+3"]),
        # Only load the given build number if it is specified
        ("0.0.1+0", "", ["0.0.1+1"]),
        # If no build number is specified, load the highest build number
        ("0.0.1", "0.0.1+4", ["0.1.0", "0.0.1+4", "0.0.1"]),
        # Build number 0 means load that version exactly
        ("0.0.1+0", "0.0.1+0", ["0.0.1+0"]),
        ("0.0.1+0", "0.0.1+0", ["0.0.1", "0.0.1+1", "1.0.0"]),
        # 1-test counts as a test version, as does 1.0
        ("", "0.0.1+0", ["0.0.1", "1-test"]),
        ("", "0.0.1+0", ["0.0.1", "1.0"]),
        # Numeric version should be prioritised over "higher" test version
        ("", "0.1.0+0", ["0.1.0", "1.0.0-rc1"]),
    ],
)
def test_version(wrapper, requested, expected, installed):
    for version in installed:
        returncode, _, _ = wrapper.run_make(
            "clean",
            "cellinstall",
            module_version=version,
        )
        assert returncode == 0

    rc, stdout, _ = run_ioc_get_output(
        wrapper.name, requested, wrapper.path / "cellMods"
    )

    if expected:
        match = re.search(
            RE_MODULE_LOADED.format(module=wrapper.name, version=re.escape(expected)),
            stdout,
        )
        assert rc == 0
        assert match
    else:
        match = re.search(
            RE_MODULE_NOT_LOADED.format(
                module=wrapper.name, required=re.escape(requested)
            ),
            stdout,
        )
        assert match
        assert rc != 0
