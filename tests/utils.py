import time

from run_iocsh import IOC


def run_ioc_get_output(module, version, cell_path):
    """
    Run an IOC and try to load the test module
    """
    with IOC(
        "-r", f"{module},{version}", "-l", cell_path, ioc_executable="iocsh"
    ) as ioc:
        time.sleep(1)
    return ioc.proc.returncode, ioc.outs, ioc.errs
