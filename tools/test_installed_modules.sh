#!/usr/bin/env bash
# Copyright (c) 2020-Present European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

REQUIRE_DIR=$(
  cd "$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"/.. || exit
  pwd
)
REQUIRE_VERSION=${REQUIRE_DIR##*/}
BASE_LOCATION=$(
  cd "${REQUIRE_DIR}"/../.. || exit
  pwd
)

if ! command -v run-iocsh >/dev/null 2>&1; then
  echo "You need to install run-iocsh to use this script" >&2 && exit 1
fi

for d in "$REQUIRE_DIR"/siteMods/*/*; do
  mod=$(dirname "$d" | xargs basename)
  ver=$(basename "$d")
  echo "Running test: $mod,$ver"
  echo "======================================================="

  if run-iocsh --base_location "${BASE_LOCATION}" --require_version "${REQUIRE_VERSION}" --delay 2 --timeout 5 "$mod,$ver"; then
    echo -e "Module $mod, version $ver: \e[32mTEST PASSED\e[0m"
  else
    echo -e "Module $mod, version $ver: \e[31mFAILED\e[0m" >&2
  fi
  echo "======================================================="
  echo ""
done
